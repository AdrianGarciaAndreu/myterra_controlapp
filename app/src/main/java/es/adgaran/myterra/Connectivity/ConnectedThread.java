package es.adgaran.myterra.Connectivity;

import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ConnectedThread extends Thread {

    private Handler handler; // handler that gets info from Bluetooth service

    private interface MessageConstants {
        public static final int MESSAGE_READ = 0;
        public static final int MESSAGE_WRITE = 1;
        public static final int MESSAGE_TOAST = 2;
    }


    private final BluetoothSocket mmSocket;
    private final InputStream mmInStream;
    private final OutputStream mmOutStream;
    private byte[] mmBuffer; // mmBuffer store for the stream



    public ConnectedThread(BluetoothSocket socket, Handler handler) {
        mmSocket = socket;

        InputStream tmpIn = null;
        OutputStream tmpOut = null;

        this.handler = handler;



        // Get the input and output streams; using temp objects because
        // member streams are final.
        try {
            tmpIn = socket.getInputStream();
        } catch (IOException e) {
            Log.e("Error", "Error occurred when creating input stream", e);
        }
        try {
            tmpOut = socket.getOutputStream();
        } catch (IOException e) {
            Log.e("Error", "Error occurred when creating output stream", e);
        }

        mmInStream = tmpIn;
        mmOutStream = tmpOut;


    }

    @Override
    public void run() {
        mmBuffer = new byte[1024];
        int numBytes; // bytes returned from read()


        Log.i("CONECTED", "Connection as client successfully established");

        // Keep listening to the InputStream until an exception occurs.
        while (true) {
            try {


                // Read from the InputStream.
                numBytes = mmInStream.read(mmBuffer);
                // Send the obtained bytes to the UI activity.
                Message readMsg = handler.obtainMessage(
                        MessageConstants.MESSAGE_READ, numBytes, -1,
                        mmBuffer);
                readMsg.sendToTarget();


                Log.i("Data I", "num bytes: "+numBytes);



            } catch (IOException e) {

                Log.d("Excepcion", "Input stream was disconnected", e);

                if(e.getMessage().contains("read return: -1")){
                    Message readMsg = handler.obtainMessage(
                            MessageConstants.MESSAGE_TOAST, -1, -1,
                            mmBuffer);
                    readMsg.sendToTarget();
                }

                break;
            }
        }
    }



    public void write(byte[] bytes) {
        try {
            mmOutStream.write(bytes);
        } catch (IOException e) { }
    }

    public void cancel() {
        try {
            mmSocket.close();
        } catch (IOException e) { }
    }




    public BluetoothSocket getMmSocket(){
        return this.mmSocket;
    }






}




