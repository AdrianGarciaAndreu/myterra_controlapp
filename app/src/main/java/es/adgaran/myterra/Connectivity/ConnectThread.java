package es.adgaran.myterra.Connectivity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.IOException;
import java.net.Socket;
import java.util.UUID;

public class ConnectThread extends  Thread {

    private final BluetoothSocket mmSocket;
    private final BluetoothDevice mmDevice;
    private final static UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");

    private String SSID, passwd;

    public BluetoothAdapter mBluetoothAdapter;
    private ConnectedThread sendingSSIDThread;

    public ConnectThread(BluetoothDevice device, BluetoothAdapter mBluetoothAdapter) {


        BluetoothSocket tmp = null;
        this.mBluetoothAdapter = mBluetoothAdapter;
        mmDevice = device;



        try {
            tmp = device.createRfcommSocketToServiceRecord(MY_UUID);
        } catch (IOException e) { }
        mmSocket = tmp;
    }


    public void run() {
        mBluetoothAdapter.cancelDiscovery();
        int att = 0;
        while(att<3){
            Log.i("CT", "Iterando: "+att);
            att++;
            try {
                mmSocket.connect();
                Log.i("CONNECTED", "connected to device "+mmDevice.getAddress());

                break;

            } catch (IOException connectException) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                try {
                    mmSocket.close();
                } catch (IOException closeException) { }
                return;
            }

        }


    }
    public void cancel() {
        try {
            mmSocket.close();
        } catch (IOException e) { }
    }


    public BluetoothSocket getMmSocket() {
        return mmSocket;
    }




}
