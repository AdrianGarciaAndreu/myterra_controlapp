package es.adgaran.myterra;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.ErrorCodes;
import com.firebase.ui.auth.IdpResponse;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Arrays;

import es.adgaran.myterra.remotes.UserGET;
import es.adgaran.myterra.remotes.UserPOST;


public class LoginActivity extends AppCompatActivity implements RemoteCallback {


    private int RC_SIGN_IN = 123; //ID del intent de login
    private FirebaseUser user;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);


        this.user = FirebaseAuth.getInstance().getCurrentUser();

        if(this.user != null){
            UserGET uGet = new UserGET(this.user.getUid(),this);
            uGet.execute();
        } else{
            SignUp();
        }


        //login();
    }



    private void login() {
        if(this.user!=null){ //Si no hay usuario logeado, se lleva a la sección de logín
             SignIn();
        } else{
            SignUp(); //Se trata de iniciar sesión
        }

    }



    private void UpdateUserLoginTime(FirebaseUser fu){
        User u =new User( this.user.getUid(), this.user.getDisplayName(), this.user.getEmail(), System.currentTimeMillis());

        // Updates user login time
        UserPOST uPost = new UserPOST(u,this);
        uPost.execute();
    }


    public void SignIn(){
        Intent i = new Intent(this, HomeActivity.class);
        /*i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK); */
        startActivity(i);
    }



    public void SignUp(){
        this.user = FirebaseAuth.getInstance().getCurrentUser();
        if (user  != null) { //Si hay usuario, se realiza el login
            login();
        } else { //En caso de no haber un usuario activo, se llama a un intent para crear usuarios

            startActivityForResult(AuthUI.getInstance()
                    .createSignInIntentBuilder()
                    .setLogo(R.drawable.m_icon_a)
                    .setTheme(R.style.LoginPersonalizado)
                    .setIsSmartLockEnabled(false)
                    .setAvailableProviders(Arrays.asList(
                            new AuthUI.IdpConfig.EmailBuilder().build(),
                            //new AuthUI.IdpConfig.FacebookBuilder().build(),
                            new AuthUI.IdpConfig.GoogleBuilder().build())

                    ).build(), RC_SIGN_IN);

        }
    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            if (resultCode == RESULT_OK) {
                this.user = FirebaseAuth.getInstance().getCurrentUser(); //Actualizo el usuario
                if(this.user!=null) {
                    UpdateUserLoginTime(this.user);
                    //SignIn();
                } else{SignUp();} //No debería de suceder

                //finish();
            } else {
                IdpResponse response = IdpResponse.fromResultIntent(data);
                if (response == null) {
                    Toast.makeText(this, "Cancelado", Toast.LENGTH_LONG).show();
                    return;
                } else if (response.getError().getErrorCode() == ErrorCodes.NO_NETWORK) {
                    Toast.makeText(this, "Sin conexión a Internet",
                            Toast.LENGTH_LONG).show();
                    return;
                } else if (response.getError().getErrorCode() == ErrorCodes.UNKNOWN_ERROR) {
                    Toast.makeText(this, "Error desconocido",
                            Toast.LENGTH_LONG).show();
                    return;
                }
            }
        }
    }



    @Override
    public void successfullOP(int requestCode, String data) {
            if(requestCode == Constants.COD_REMOTE_USER_POST){
                login();
                //SignIn();
            }

            if(requestCode == Constants.COD_REMOTE_USER_GET){
                User u =new User( this.user.getUid(), this.user.getDisplayName(), this.user.getEmail(), System.currentTimeMillis());

                // Updates user login time
                UserPOST uPost = new UserPOST(u,this);
                uPost.execute();

            }
    }

    @Override
    public void failedOP(int requestCode) {
        Toast.makeText(this,"Login Failed, server problems",Toast.LENGTH_LONG).show();

    }



}
