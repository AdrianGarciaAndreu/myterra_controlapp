package es.adgaran.myterra.adapters;

import android.bluetooth.BluetoothDevice;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import es.adgaran.myterra.R;

public class PairedDevicesAdapter extends  RecyclerView.Adapter<PairedDevicesAdapter.ViewHolder> {

    protected List<BluetoothDevice> devices;

    public PairedDevicesAdapter(List<BluetoothDevice> devices){
        this.devices = devices;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name, address,_pos;
        public ImageView image, img_connected;

        public ViewHolder(View itemView){
            super(itemView);
            name = itemView.findViewById(R.id.sync_devName);
            address = itemView.findViewById(R.id.sync_devAddress);

            image =  itemView.findViewById(R.id.sync_devImage);
            img_connected =  itemView.findViewById(R.id.sync_devImageConnected);

            _pos = itemView.findViewById(R.id.sync_devPos);
        }

    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.paired_element, parent, false);
        v.setOnClickListener(onClickListener);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        String _name = this.devices.get(position).getName();
        String _address = this.devices.get(position).getAddress();

        if(_name==null){ _name="(No name)"; }
        holder._pos.setText(String.valueOf(position));


        holder.name.setText(_name);
        holder.address.setText(_address);

        holder.image.setOnClickListener(onImageClickListener);

    }

    @Override
    public int getItemCount() {
        return this.devices.size();
    }



    // Eventos

    protected View.OnClickListener onClickListener;

    public void setOnItemClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }



    protected View.OnClickListener onImageClickListener;
    public void setOnImageClickListener(View.OnClickListener onClickListener){
        onImageClickListener = onClickListener;

    }





}
