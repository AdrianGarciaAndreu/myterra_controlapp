package es.adgaran.myterra.adapters;

import android.bluetooth.BluetoothDevice;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import es.adgaran.myterra.R;
import es.adgaran.myterra.pojo.Node;

public class DeviceAdapter extends RecyclerView.Adapter<DeviceAdapter.ViewHolder> {

    protected List<Node> devices;

    public DeviceAdapter(List<Node> devices){
        this.devices = devices;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name, time, HR, temp, moist, lIntensity, _pos, txt_net;
        public ImageView img_led, img_color, img_uv, img_name, img_white, img_netStatus, img_net;

        public ViewHolder(View itemView){
            super(itemView);
            name = itemView.findViewById(R.id.txt_name);
            time = itemView.findViewById(R.id.txt_time);
            HR = itemView.findViewById(R.id.txt_hr);
            temp = itemView.findViewById(R.id.txt_temp);
            moist = itemView.findViewById(R.id.txt_moist);
            lIntensity = itemView.findViewById(R.id.txt_intensity);
            _pos = itemView.findViewById(R.id.txt_devPos);
            txt_net = itemView.findViewById(R.id.lbl_netStatus);

            img_led =  itemView.findViewById(R.id.icon_light);
            img_color =  itemView.findViewById(R.id.icon_light_color);
            img_uv =  itemView.findViewById(R.id.icon_uv);
            img_name = itemView.findViewById(R.id.img_setName);
            img_white = itemView.findViewById(R.id.icon_light_white);
            img_netStatus = itemView.findViewById(R.id.img_requestNetStatus);
            img_net = itemView.findViewById(R.id.img_netStatus);



        }

    }


    @NonNull
    @Override
    public DeviceAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.devices_element, parent, false);
        v.setOnClickListener(onClickListener);
        return new DeviceAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull DeviceAdapter.ViewHolder holder, int position) {

        Node n = this.devices.get(position);

        String _name = n.getName();
        String _time = n.getMomment();

        //Calendar c = new GregorianCalendar();
        //c.setTime(new Date(_time));

        //String _momment = c.get(Calendar.DAY_OF_MONTH)+"/"+c.get(Calendar.MONTH)+"/"+c.get(Calendar.YEAR)+" "+ c.get(Calendar.HOUR)+"/"+c.get(Calendar.MINUTE)+"/"+c.get(Calendar.SECOND);
        String _momment = Epoch2DateString(String.valueOf(_time));

        if(_name==null){ _name="(No name)"; }
        holder.time.setText(_momment);
        holder.name.setText(_name);

        holder.HR.setText(String.valueOf(n.getHr()) +"%" );
        holder.temp.setText(String.valueOf(n.getTemp()) + "º C");
        holder.moist.setText(String.valueOf(n.getMoist()) +"%");
        holder.lIntensity.setText(String.valueOf(n.getLightInt()) +"%");
        holder._pos.setText(String.valueOf(position));

        holder.img_color.setImageResource(R.drawable.colorpicker);
        if(n.isLedState()){
            holder.img_led.setImageResource(R.drawable.ideaon);
        } else{
            holder.img_led.setImageResource(R.drawable.lightoff);
        }

        if(n.isUvState()){
            holder.img_uv.setImageResource(R.drawable.uvon);
        } else{
            holder.img_uv.setImageResource(R.drawable.uvoff);
        }


        if(n.getNodeIsConnected()){
            holder.img_net.setImageResource(R.drawable.switch_on);
            holder.txt_net.setText("Connected");
        } else{
            holder.img_net.setImageResource(R.drawable.switch_off);
            holder.txt_net.setText("Not connected");
        }


        holder.img_led.setOnClickListener(onImageLEDClickListener);
        holder.img_uv.setOnClickListener(onImageUVClickListener);
        holder.img_color.setOnClickListener(onImageColorClickListener);
        holder.img_name.setOnClickListener(onImageNameClickListener);
        holder.img_white.setOnClickListener(onImageWhiteClickListener);

        holder.img_netStatus.setOnClickListener(onNetStatusClickListener);

    }

    @Override
    public int getItemCount() {
        return this.devices.size();
    }



    // Eventos

    protected View.OnClickListener onClickListener;

    public void setOnItemClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }




    protected View.OnClickListener onImageLEDClickListener;
    public void setOnImageLEDClickListener(View.OnClickListener onClickListener){
        onImageLEDClickListener = onClickListener;

    }

    protected View.OnClickListener onImageUVClickListener;
    public void setOnImageUVClickListener(View.OnClickListener onClickListener){
        onImageUVClickListener = onClickListener;

    }

    protected View.OnClickListener onImageColorClickListener;
    public void setOnImageColorClickListener(View.OnClickListener onClickListener){
        onImageColorClickListener = onClickListener;

    }


    protected View.OnClickListener onImageNameClickListener;
    public void setOnImageNameClickListener(View.OnClickListener onClickListener){
        onImageNameClickListener = onClickListener;

    }



    protected View.OnClickListener onImageWhiteClickListener;
    public void setOnImageWhiteClickListener(View.OnClickListener onClickListener){
        onImageWhiteClickListener = onClickListener;

    }



    protected View.OnClickListener onNetStatusClickListener;
    public void setOnNetStatusClickListener(View.OnClickListener onClickListener){
        onNetStatusClickListener = onClickListener;

    }





    public static String Epoch2DateString(String epochSeconds) {
      //  Date updatedate = new Date(Integer.parseInt(epochSeconds) * 1000L);
      //  SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
      //  return format.format(updatedate);

        return epochSeconds;
    }




}
