package es.adgaran.myterra.adapters;

import android.bluetooth.BluetoothDevice;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import es.adgaran.myterra.R;

public class NotPairedDevicesAdapter extends  RecyclerView.Adapter<NotPairedDevicesAdapter.ViewHolder> {

    protected List<BluetoothDevice> devices;

    public NotPairedDevicesAdapter(List<BluetoothDevice> devices){
        this.devices = devices;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name, address;
        public TextView tvPos;

        public ViewHolder(View itemView){
            super(itemView);
            name = itemView.findViewById(R.id.sync_new_devName);
            address = itemView.findViewById(R.id.sync_new_devAddress);
            tvPos = itemView.findViewById(R.id.sync_new_devPos);
        }

    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.not_paired_element, parent, false);
        v.setOnClickListener(onClickListener);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        String _name = this.devices.get(position).getName();
        String _address = this.devices.get(position).getAddress();

        if(_name==null){ _name="(No name)"; }

        holder.name.setText(_name);
        holder.address.setText(_address);
        holder.tvPos.setText(""+position);


    }

    @Override
    public int getItemCount() {
        return this.devices.size();
    }



    // Eventos

    protected View.OnClickListener onClickListener;

    public void setOnItemClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }





}
