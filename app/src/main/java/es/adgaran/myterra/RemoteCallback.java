package es.adgaran.myterra;

public interface RemoteCallback {

    void successfullOP(int requestCode, String data); // Cuando una tarea termine satisfactoriamente
    void failedOP(int requestCode); // Cuando una tarea termine con un fallo


}
