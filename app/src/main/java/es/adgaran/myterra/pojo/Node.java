package es.adgaran.myterra.pojo;

import java.util.ArrayList;
import java.util.List;

public class Node {


    private String name;
    private String guid;
    private int hr, moist, temp, lightInt;
    private boolean ledState, uvState;
    private List<Integer> ledColor = new ArrayList<>(3);
    private String momment;
    private String hwMomment;


    private String id;
    private int wInt; //whiteness intensity (rgbW)

    private boolean statusRequest;

    public Node(){

    }

    public Node(String name, String guid, int hr, int moist, int temp, int lightInt, boolean ledState, boolean uvState, List<Integer> ledColor, String momment, String id) {
        this.name = name;
        this.guid = guid;
        this.hr = hr;
        this.moist = moist;
        this.temp = temp;
        this.lightInt = lightInt;
        this.ledState = ledState;
        this.uvState = uvState;
        this.ledColor = ledColor;
        this.momment = momment;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public int getHr() {
        return hr;
    }

    public void setHr(int hr) {
        this.hr = hr;
    }

    public int getMoist() {
        return moist;
    }

    public void setMoist(int moist) {
        this.moist = moist;
    }

    public int getTemp() {
        return temp;
    }

    public void setTemp(int temp) {
        this.temp = temp;
    }

    public int getLightInt() {
        return lightInt;
    }

    public void setLightInt(int lightInt) {
        this.lightInt = lightInt;
    }

    public boolean isLedState() {
        return ledState;
    }

    public void setLedState(boolean ledState) {
        this.ledState = ledState;
    }

    public boolean isUvState() {
        return uvState;
    }

    public void setUvState(boolean uvState) {
        this.uvState = uvState;
    }

    public List<Integer> getLedColor() {
        return ledColor;
    }

    public void setLedColor(List<Integer> ledColor) {
        this.ledColor = ledColor;
    }

    public String getMomment() {
        return momment;
    }

    public void setMomment(String momment) {
        this.momment = momment;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getwInt() {
        return wInt;
    }

    public void setwInt(int wInt) {
        this.wInt = wInt;
    }


    public boolean isStatusRequest() {
        return statusRequest;
    }

    public void setStatusRequest(boolean statusRequest) {
        this.statusRequest = statusRequest;
    }


    public String getHwMomment() {
        return hwMomment;
    }

    public void setHwMomment(String hwMomment) {
        this.hwMomment = hwMomment;
    }

    public boolean getNodeIsConnected(){
        boolean isConnected = false;

        //TODO: Ver que hacer

        /*
        String lastUpdate = this.getHwMomment();
        long diff_s = ((System.currentTimeMillis()/1000) - lastUpdate);

        if(diff_s>50){ }
        else{
            isConnected = true;
        }

        */
        return isConnected;
    }


}
