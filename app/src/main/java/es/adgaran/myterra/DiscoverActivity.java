package es.adgaran.myterra;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import es.adgaran.myterra.adapters.NotPairedDevicesAdapter;

public class DiscoverActivity extends AppCompatActivity {

    private BluetoothAdapter bta;
    private List<BluetoothDevice> devices;

    private RecyclerView rv;
    private NotPairedDevicesAdapter adapter;
    private Switch btnDiscover;
    private ProgressDialog dialog;


    //Selected
    private BluetoothDevice device;
    private String device_address;
    private boolean device_bonded;

    private final int REQUEST_ENABLE_BT = 124;
    private final int REQUEST_ENABLE_GPS = 125;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discover);

        Toolbar tb = findViewById(R.id.toolbar);
        setSupportActionBar(tb);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Discover devices");


        //Se registra el intent de recepción
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        filter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
        registerReceiver(receiver, filter);


        //Populate ReciclerView
        populateReciclerView();


        //Register event
        this.btnDiscover = findViewById(R.id.btn_discover_selected);
        btnDiscover.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    if(bta.isDiscovering()){
                        bta.cancelDiscovery();
                    }
                    Log.i("BTE", "START DISCOVERING..");
                    devices.clear(); adapter.notifyDataSetChanged();

                    if(bta.startDiscovery()){
                    }else{
                        Log.e("BTE Discover fail", "FAILED DISCOVERING");
                    }

                }else{
                    if(bta.isDiscovering()){
                        bta.cancelDiscovery();
                    }
                }
            }
        });


        //Check Bluetooth and search devices
        checkBLE();
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }




    //Carga el Listado de dispositivos por defecto (vacío)
    private void populateReciclerView(){

        this.devices = new ArrayList<>();
        this.rv = findViewById(R.id.sync_rv);

        this.adapter = new NotPairedDevicesAdapter(this.devices);
        this.rv.setLayoutManager(new LinearLayoutManager(this));
        this.rv.setHasFixedSize(false);
        this.rv.setAdapter(this.adapter);


        this.adapter.setOnItemClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startDialog();

                TextView tv = v.findViewById(R.id.sync_new_devPos);
                device = devices.get(Integer.parseInt(tv.getText().toString()));
                Log.i("DEVICE", "DEVICE SELECTED "+device.getName() + "("+device.getAddress()+")");


                boolean bond = device.createBond(); //Try to create a bond


            }
        });



    }


    //Checks BLE, and search for devices
    private void checkBLE(){

        this.bta = BluetoothAdapter.getDefaultAdapter();
        if(this.bta!=null){

            if(!this.bta.isEnabled()){

                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);

            } else {

                searchForDevices();
                checkGPS();
            }

        }
    }



    private void checkGPS(){


        Log.i("CHEKING", "cheking for location service");

        // Android V 10 o >

        String deviceOs = "1";
        deviceOs = Build.VERSION.RELEASE;
        //Log.i("V", deviceOs);


        if(Float.parseFloat( deviceOs )>= 10.0 && !isLocationEnabled(this)){
            Intent gpsOptionsIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivityForResult(gpsOptionsIntent, REQUEST_ENABLE_GPS);

        } else{
            searchForDevices();
        }


    }




    //Enables discovery once by default
    private void searchForDevices(){

        if(bta.isEnabled()){
            this.btnDiscover.setChecked(true); //triggers one discovery event
        }


    }



    //Receptor de boradcasts para el bluetooth
    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                String deviceName = device.getName();
                String deviceHardwareAddress = device.getAddress(); // MAC address

                Log.i("BTE DISCOVERED!", deviceName+" ("+deviceHardwareAddress+")");

                devices.add(device);
                adapter.notifyItemInserted(devices.size()-1);
            }

            //IF Discovery finished
            if(BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)){
                Log.i("BTE", "STOPS DISCOVERING..");
                btnDiscover.setChecked(false);
            }

            if(BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)) {
                Log.i("BOND", "bonding changed");
                int state = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, -1);

                switch (state){
                    case BluetoothDevice.BOND_BONDED:
                        dialog.dismiss();
                        closeActivity();
                    break;
                    case BluetoothDevice.BOND_NONE:
                        dialog.dismiss();
                        Log.e("BOND", "FAIL ON BONDING OR CANCELED");
                    break;
                }

            }



        }
    };


    //Closes the activity
    private void closeActivity(){
        finish();
    }

    private void startDialog(){
        dialog = dialog.show(DiscoverActivity.this, "", "Pairing...", true);

    }


    // ACTIVITY FLOW


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_ENABLE_BT){
            if(resultCode==RESULT_OK){
                checkGPS();
            }
        }

        if(requestCode == REQUEST_ENABLE_GPS){
            if(resultCode==RESULT_OK){
                searchForDevices();
            }
        }



    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        unregisterReceiver(receiver);
        this.bta.cancelDiscovery();

    }



    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        }else{
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }


    }



}