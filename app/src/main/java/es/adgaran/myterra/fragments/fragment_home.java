package es.adgaran.myterra.fragments;

import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.skydoves.colorpickerview.ColorEnvelope;
import com.skydoves.colorpickerview.ColorPickerDialog;
import com.skydoves.colorpickerview.listeners.ColorEnvelopeListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.adgaran.myterra.Constants;
import es.adgaran.myterra.R;
import es.adgaran.myterra.RemoteCallback;
import es.adgaran.myterra.adapters.DeviceAdapter;
import es.adgaran.myterra.pojo.Node;
import es.adgaran.myterra.remotes.NodesGET;
import es.adgaran.myterra.remotes.NodesPOST;
import es.adgaran.myterra.services.BGService;

public class fragment_home extends Fragment implements RemoteCallback {


    private List<Node> nodos;
    private RecyclerView rv;
    private DeviceAdapter adapter;
    private AlertDialog whiteDialog;

    private HashMap<String, Boolean> nodesCheked;
    private Intent onCancelIntent;
    private PendingIntent onDismissPendingIntent;

    private MyBroadcastReceiver myBroadcastReceiver;


   // private FirebaseFirestore db;


    private final String CHANNEL_ID = "MyTerra";
    private final int hrLimit = 50;
    private final int moistLimit = 65;
    private final int tempLimit = 35;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);
        View root = inflater.inflate(R.layout.fragment_home, container, false);



        nodos = new ArrayList<>();
        nodesCheked = new HashMap<>();

        //onCancelIntent = new Intent(getContext(), OnCancelBroadcastReceiver.class);
        //onDismissPendingIntent = PendingIntent.getBroadcast(this.getContext().getApplicationContext(), 0, onCancelIntent, 0);

        this.rv = root.findViewById(R.id.rv_devices);
        this.adapter = new DeviceAdapter(this.nodos);

        this.rv.setLayoutManager(new LinearLayoutManager(getContext()));
        this.rv.setHasFixedSize(false);
        this.rv.setAdapter(this.adapter);



        this.adapter.setOnImageLEDClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Device targeted
                View parent = (View)v.getParent();
                TextView tv = parent.findViewById(R.id.txt_devPos);
                int _pos = Integer.parseInt(tv.getText().toString());

                Log.i("Touch", "Posición: "+_pos);

                Node n = nodos.get(_pos);
                changeLightState(n);

            }
        });


        this.adapter.setOnImageUVClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Device targeted
                View parent = (View)v.getParent();
                TextView tv = parent.findViewById(R.id.txt_devPos);
                int _pos = Integer.parseInt(tv.getText().toString());

                Log.i("Touch", "Posición: "+_pos);

                Node n = nodos.get(_pos);
                changeUVState(n);


            }
        });

        this.adapter.setOnImageColorClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Device targeted
                View parent = (View)v.getParent();
                TextView tv = parent.findViewById(R.id.txt_devPos);
                int _pos = Integer.parseInt(tv.getText().toString());

                Log.i("Touch", "Posición: "+_pos);

                final Node n = nodos.get(_pos);


                ColorPickerDialog.Builder builder = new ColorPickerDialog.Builder(getContext(), AlertDialog.THEME_DEVICE_DEFAULT_DARK)
                        .setTitle("Choose LED Coloring")
                        .setPreferenceName("Color picker")
                        .setPositiveButton("Change color",
                                new ColorEnvelopeListener() {
                                    @Override
                                    public void onColorSelected(ColorEnvelope envelope, boolean fromUser) {

                                        int[] _colors = envelope.getArgb();
                                        Integer[] colors = new Integer[_colors.length-1];

                                        //Empiezo en la posición 1, para ignorar el canal Alpha
                                        for(int i=1; i<_colors.length; i++){
                                            colors[i-1] = new Integer(_colors[i]);
                                        }

                                        changeLedColor(n, colors);
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                })
                        .attachAlphaSlideBar(false) // default is true. If false, do not show the AlphaSlideBar.
                        .attachBrightnessSlideBar(true);  // default is true. If false, do not show the BrightnessSlideBar.

                builder.show();
            }



        });


        this.adapter.setOnImageWhiteClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                View parent = (View)v.getParent();
                TextView tv = parent.findViewById(R.id.txt_devPos);
                int _pos = Integer.parseInt(tv.getText().toString());

                Log.i("Touch", "Posición: "+_pos);

                final Node n = nodos.get(_pos);

                final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                ViewGroup viewGroup = getActivity().findViewById(android.R.id.content);
                final View dialogView = LayoutInflater.from(v.getContext()).inflate(R.layout.dialog_colorpicker, viewGroup, false);
                SeekBar sb = dialogView.findViewById(R.id.white_bar);
                sb.setProgress(n.getwInt());

                builder.setView(dialogView);
                builder.setPositiveButton("Select", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                            SeekBar sb = dialogView.findViewById(R.id.white_bar);
                            int p = sb.getProgress();
                            changeNodeWInt(n, p);
                            Log.d("whiteness", p+"");
                    }
                });

                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        whiteDialog.dismiss();
                    }
                });
                whiteDialog = builder.create();
                whiteDialog.show();

            }
        });


        this.adapter.setOnImageNameClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Device targeted
                View parent = (View)v.getParent();
                TextView tv = parent.findViewById(R.id.txt_devPos);
                int _pos = Integer.parseInt(tv.getText().toString());

                //Log.i("Touch", "Posición: "+_pos);

                final Node n = nodos.get(_pos);



                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Change node name");

                LayoutInflater inflater = getLayoutInflater();
                final View dView = inflater.inflate(R.layout.dialog_config_name, null);

                alertDialog.setView(dView);

                alertDialog.setPositiveButton("Confirm",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                EditText et_name = (EditText)dView.findViewById(R.id.config_name_data);
                                String _name = et_name.getText().toString();

                                if(_name.length()>0){
                                    changeNodeName(n,_name);
                                } else{
                                    Toast.makeText(getContext(),"At least 1 character is required", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });

                alertDialog.setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.cancel();
                            }
                        });

                alertDialog.show();




            }
        });




        this.adapter.setOnNetStatusClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Device targeted
                View parent = (View)v.getParent();
                TextView tv = parent.findViewById(R.id.txt_devPos);
                int _pos = Integer.parseInt(tv.getText().toString());

                Log.i("Touch", "Posición: "+_pos);

                Node n = nodos.get(_pos);
                n.setStatusRequest(true);

             //   db.collection("nodes").document(n.getId()).set(n);

            }
        });




        return root;
    }




    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



       // this.db = FirebaseFirestore.getInstance();
        getMyDevices();


        // Not used any more probably

        /*
        // If there are nodes
        Handler h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(nodos.size()>0){
                    checkStatus();
                }
            }
        }, 5000);


         */



        //Start MyIntentService
        Intent intentMyIntentService = new Intent(getContext(), BGService.class);
        intentMyIntentService.putExtra(BGService.EXTRA_KEY_IN, "Starting...");
        getContext().startService(intentMyIntentService);


        // background receiver
        this.myBroadcastReceiver = new MyBroadcastReceiver();


        //register BroadcastReceiver
        IntentFilter intentFilter = new IntentFilter(BGService.ACTION_MyIntentService);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        getContext().registerReceiver(myBroadcastReceiver, intentFilter);



    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }




    //Querys
    private void getMyDevices(){
        String guid;
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        try{
            Log.d("GUID", user.getUid());
            if(user.getUid().length()>0){
                guid = user.getUid();

                Log.i("SEARCHING", "Searching for nodes");


                // GET REQUEST
                NodesGET nodesGet = new NodesGET(guid, (RemoteCallback) this);
                nodesGet.execute();

            }

        } catch (NullPointerException ex){
            ex.printStackTrace();
        }


    }



    private void notifyLimits(Node n){


        boolean notified = false;
        //checks if already notified node
        for(Map.Entry<String, Boolean> entry : nodesCheked.entrySet()) {
            String key = entry.getKey();
            boolean value = entry.getValue();
            if(key.equals(n.getId())){
                if(value){notified=true;}
            }

        }


        if(!notified){
            String msg = "";
            if(n.getHr() <= hrLimit){
                msg +="Your terrarium maybe needs some care (HR level)\n";
            }
            if(n.getMoist() <= moistLimit){
                msg +="Your terrarium maybe needs some care (Moist level)\n";
            }
            if(n.getTemp() >= tempLimit){
                msg +="Your terrarium maybe needs some care (Temperature)\n";
            }

            NotificationManager mNotificationManager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);

            if(msg.length()>0){
                String _id = n.getId();

                NotificationChannel mChannel = null;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    mChannel = new NotificationChannel("Prueba", "Canal de prueba", NotificationManager.IMPORTANCE_HIGH);
                }

                NotificationCompat.Builder Builder = new NotificationCompat.Builder(getContext(), "Prueba")
                        .setSmallIcon(R.drawable.ic_stat_onesignal_default)
                        .setContentTitle("MyTerra")
                        .setContentText(msg)
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        .setChannelId("Prueba");

                Builder.setDeleteIntent(onDismissPendingIntent);


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    mNotificationManager.createNotificationChannel(mChannel);
                }

                mNotificationManager.notify(_id.hashCode(), Builder.build());
                nodesCheked.put(n.getId(),true);

            }

        }
    }








    // Report to the user if a node is connected or no getting the last update time
    Handler delayhandler = new Handler();
    Runnable run;
    private void checkStatus(){

        this.run = new Runnable() {
            @Override
            public void run() {
                runCheckStatus();

            }
        };
        delayhandler.postDelayed(run, 20000);

    }




    void runCheckStatus(){
        Log.i("Checking", "Checking status");
        getMyDevices();
        delayhandler.postDelayed(run,20000);
    }




    private void changeLightState(Node n){

        if(n.isLedState()){
            n.setLedState(false);
        } else{n.setLedState(true);}


        n.getNodeIsConnected();

        NodesPOST nodesPost = new NodesPOST(n, this);
        nodesPost.execute();

    }


    private void changeUVState(Node n){

        if(n.isUvState()){
            n.setUvState(false);
        } else{n.setUvState(true);}


        n.getNodeIsConnected();

        NodesPOST nodesPost = new NodesPOST(n, this);
        nodesPost.execute();

    }


    private void changeLedColor(Node n, Integer[] colors){

        n.setLedColor(Arrays.asList(colors));

        n.getNodeIsConnected();

        NodesPOST nodesPost = new NodesPOST(n, this);
        nodesPost.execute();

    }



    private void changeNodeName(Node n, String _name){

        n.setName(_name);

        n.getNodeIsConnected();

        NodesPOST nodesPost = new NodesPOST(n, this);
        nodesPost.execute();

    }



    private void changeNodeWInt(Node n, int intensity){

        n.setwInt(intensity);

        n.getNodeIsConnected();

        NodesPOST nodesPost = new NodesPOST(n, this);
        nodesPost.execute();
    }

    @Override
    public void onDestroy() {
        delayhandler.removeCallbacks(run);

        //un-register BroadcastReceiver
        getContext().unregisterReceiver(myBroadcastReceiver);

        super.onDestroy();
    }




    public void successfullOP(int requestCode, String data){
        // When remote operation works...
        switch (requestCode){
            case Constants.COD_REMOTE_NODE_GET:
                    // Do some stuff
                    Log.d("NODE", "JSON Obtained: "+data);

                    // Populate Nodes List
                try {
                    nodos.clear();

                    JSONObject dataJSON = new JSONObject(data);
                        JSONArray dataJSONArray = dataJSON.getJSONArray("datos"); //Datos
                       Log.d("NODE", "JSON Obtained data: "+dataJSONArray);

                        for (int i=0; i<dataJSONArray.length(); i++){
                            JSONObject nodeJSON = dataJSONArray.getJSONObject(i);
                             Node n = JsonNode2Pojo(nodeJSON);
                             this.nodos.add(n);

                             Log.d("NODE", "Added node: "+nodos.get(nodos.size()-1).getId());
                        }
                        // add to the adapter

                        adapter.notifyDataSetChanged();


                }

                 catch (JSONException e) {
                    e.printStackTrace();
                }


                break;
            case Constants.COD_REMOTE_NODE_POST:
                    // Do some other stuff

                adapter.notifyDataSetChanged();

                break;
        }
    }

    public void failedOP(int requestCode){

        switch (requestCode){
            case Constants.COD_REMOTE_NODE_GET:
                // Do some stuff
                break;
            case Constants.COD_REMOTE_NODE_POST:
                // Do some other stuff
                break;
        }


    }


    /**
     * Converts NODE JSON to NODE POJO
     * to be easily processed
     */
    private Node JsonNode2Pojo(JSONObject nodeIn){
        Node nodeOut = new Node();

        try {

            nodeOut.setGuid(nodeIn.getString("guid"));
            nodeOut.setId(nodeIn.getString("macAddress"));
            nodeOut.setHr(nodeIn.getInt("hr"));
            nodeOut.setHwMomment(nodeIn.getString("hwMomment"));
            ArrayList<Integer> ledColors =  new ArrayList<Integer>(3);
                ledColors.add(0,nodeIn.getInt("ledColorR"));
                ledColors.add(1,nodeIn.getInt("ledColorG"));
                ledColors.add(2,nodeIn.getInt("ledColorB"));
            nodeOut.setLedColor(ledColors);

            nodeOut.setLedState(tiniIntToBool(nodeIn.getInt("ledState")));
            nodeOut.setLightInt(nodeIn.getInt("lightInt"));
            nodeOut.setMoist(nodeIn.getInt("moist"));
            nodeOut.setTemp(nodeIn.getInt("temp"));
            nodeOut.setName(nodeIn.getString("name"));
            nodeOut.setMomment(nodeIn.getString("momment"));
            nodeOut.setStatusRequest(tiniIntToBool(nodeIn.getInt("statusRequest")));
            nodeOut.setUvState(tiniIntToBool(nodeIn.getInt("uvState")));
            nodeOut.setwInt(nodeIn.getInt("wInt"));


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return nodeOut;
    }


    private boolean tiniIntToBool(int value){
        boolean result = false;

        if(value==1){ result = true;}

        return result;

    }


    @Override
    public void onStart() {
        super.onStart();


       // getContext().startService(bgService);

    }


    @Override
    public void onStop() {
        super.onStop();

        //getContext().stopService(bgService);
    }


    /**
     * Receiver to get data from background service,
     * this service reads the data from the REST API periodically
     */
    public class MyBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            // If service sends data...
            if(intent.getStringExtra(BGService.EXTRA_KEY_OUT)!=null){
                String result = intent.getStringExtra(BGService.EXTRA_KEY_OUT); // Gets the data

               // Toast.makeText(getContext(),result,Toast.LENGTH_SHORT).show();
                successfullOP(Constants.COD_REMOTE_NODE_GET,result); // Updates the UI

            }
        }


    }





}
