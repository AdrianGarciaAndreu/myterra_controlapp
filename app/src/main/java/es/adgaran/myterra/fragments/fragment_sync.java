package es.adgaran.myterra.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import es.adgaran.myterra.Connectivity.ConnectThread;
import es.adgaran.myterra.Connectivity.ConnectedThread;
import es.adgaran.myterra.DiscoverActivity;
import es.adgaran.myterra.HomeActivity;
import es.adgaran.myterra.R;
import es.adgaran.myterra.adapters.PairedDevicesAdapter;

import static android.app.Activity.RESULT_OK;

public class fragment_sync extends Fragment {

    private BluetoothAdapter bta;
    private List<BluetoothDevice> devices;
    private BluetoothManager mbluetoothManager;


    private ConnectThread ct;
    private ConnectedThread sendingSSIDThread;


    //UI
    private ConstraintLayout constraintLayout;
    private TextView titleText;
    private Switch bteSwitch;
    private Button discoverBtn;
    private ProgressDialog dialog;

    private RecyclerView rv;
    private PairedDevicesAdapter adapter;
    private int bte_choosed_pos;

    private BluetoothDevice device = null; //device escogido
    private BluetoothGatt devgatt = null;



    private final int REQUEST_ENABLE_BT = 124;
    private final int REQUEST_ENABLE_GPS = 125;
    private final int DISCOVER_ACTIVITY = 210;

    private final String NET_OK = "[ACK_OK]";
    private final String NET_CONN_FAIL = "[ACK_FAIL]";
    private final String NET_STATUS = "[STATUS]";
    //private final String NET_DEVICE_FAIL = "";

    private boolean successfullACK = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_sync, container, false);

        this.constraintLayout = root.findViewById(R.id.fragment_sync_layoutID);
        this.mbluetoothManager = (BluetoothManager)getActivity().getSystemService(Context.BLUETOOTH_SERVICE);


        this.titleText = root.findViewById(R.id.txt_sync_text);
        this.bteSwitch = root.findViewById(R.id.btn_sync_on);
        this.discoverBtn = root.findViewById(R.id.btn_discover);
        this.rv = root.findViewById(R.id.sync_rv_paired);

        this.devices = new ArrayList<>();
        this.adapter = new PairedDevicesAdapter(this.devices);

        this.rv.setHasFixedSize(false);
        this.rv.setLayoutManager(new LinearLayoutManager(getContext()));
        this.rv.setAdapter(this.adapter);


        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);

        getActivity().registerReceiver(receiver, filter);




        this.adapter.setOnItemClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Device targeted
                TextView pos = v.findViewById(R.id.sync_devPos);
                bte_choosed_pos = Integer.parseInt(pos.getText().toString());
                device = devices.get(bte_choosed_pos);

                establishConnection();

                Handler h = new Handler();
                h.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        if(sendingSSIDThread.getMmSocket()!=null){
                            if(sendingSSIDThread.getMmSocket().isConnected()){
                                reportDeviceStatus();
                            }
                        }

                    }
                }, 5000);

            }
        });

        this.adapter.setOnImageClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Device targeted
                View parent = (View)v.getParent();
                TextView pos = parent.findViewById(R.id.sync_devPos);

                bte_choosed_pos = Integer.parseInt(pos.getText().toString());
                device = devices.get(bte_choosed_pos);

                createDialogToSetUpNetworks();

            }
        });



        Button btnDiscover = root.findViewById(R.id.btn_discover);
        btnDiscover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(getContext(), DiscoverActivity.class);
                startActivityForResult(intent, DISCOVER_ACTIVITY);// Activity is started with requestCode 2

            }
        });


        return root;
    }


    @Override
    public void onStart() {
        super.onStart();
        //Busqueda de dispositivos

        this.bta = BluetoothAdapter.getDefaultAdapter();
        if(this.bta!=null) {

            if (bta.isEnabled()) {
                this.bteSwitch.setChecked(true);
                //readBTE();
                checkGPS();
            }


                this.bteSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);

                            //readBTE();

                        } else {
                            bta.cancelDiscovery();
                            bta.disable();
                            getActivity().unregisterReceiver(receiver);

                            devices.clear();
                            adapter.notifyDataSetChanged();
                        }
                    }
                });


        }




    }


    private void checkGPS(){


        Log.i("CHEKING", "cheking for location service");

        // Android V 10 o >

        String deviceOs = "1";
        deviceOs = Build.VERSION.RELEASE;
        //Log.i("V", deviceOs);


        if(Float.parseFloat( deviceOs )>= 10.0 && !isLocationEnabled(getContext())){
            Intent gpsOptionsIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivityForResult(gpsOptionsIntent, REQUEST_ENABLE_GPS);

        } else{
            readBTE();
        }


    }


    private void readBTE(){

        //Bluetooth activado
        if(bta.isEnabled()){


            Set<BluetoothDevice> pairedDevices = bta.getBondedDevices();
            if (pairedDevices.size() > 0) {
                // There are paired devices. Get the name and address of each paired device.
                for (BluetoothDevice device : pairedDevices) {
                    String deviceName = device.getName();
                    String deviceHardwareAddress = device.getAddress(); // MAC address

                    if(deviceName.contains("MyTerra")){ //Just adds to the list the "MyTerra Terrariums
                        this.devices.add(device);
                    }
                }
                    this.adapter.notifyDataSetChanged();
            }

        }

    }


    //Receptor de boradcasts para el bluetooth
    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Discovery has found a device. Get the BluetoothDevice
                // object and its info from the Intent.
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                String deviceName = device.getName();
                String deviceHardwareAddress = device.getAddress(); // MAC address

                Log.i("BTE DISCOVERED!", deviceName+" ("+deviceHardwareAddress+")");
            }
        }
    };





    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_ENABLE_BT){
            if(resultCode == RESULT_OK){
                checkGPS();
            }
        }

        if(requestCode == REQUEST_ENABLE_GPS){
            if(resultCode == RESULT_OK){
                readBTE();
            }
        }

    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        this.devices.clear();
        this.adapter.notifyDataSetChanged();
    }

    @Override
    public void onPause() {
        super.onPause();

        this.devices.clear();
        this.adapter.notifyDataSetChanged();

    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        getActivity().unregisterReceiver(receiver);

        if(this.bta.isEnabled()){
        this.bta.cancelDiscovery(); cleanConnection();

        //getActivity().unregisterReceiver(receiver);

        }


    }




    public void createDialogToSetUpNetworks(){
        // Configurar RED de un nodo

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle("Set up WiFi network");

        LayoutInflater inflater = this.getLayoutInflater();
        final View dView = inflater.inflate(R.layout.dialog_config_network, null);

       alertDialog.setView(dView);

        alertDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        EditText et_ssid = (EditText)dView.findViewById(R.id.config_ssid_data);
                        String ssid = et_ssid.getText().toString();


                        EditText et_pass = (EditText)dView.findViewById(R.id.config_pass_data);
                        String pass = et_pass.getText().toString();

                        if(ssid.toString().length()<1 || pass.toString().length()<1){
                            Toast.makeText(getContext(),"Network fields should not be empty", Toast.LENGTH_LONG).show();
                        }else{
                            //Reset Threads
                            if(ct!=null){
                                if(sendingSSIDThread!=null){
                                    sendingSSIDThread.cancel();
                                    sendingSSIDThread = null; }
                                ct.cancel();
                                ct = null;

                                if(devgatt!=null){devgatt.close();}
                            }
                            sendConfig(ssid, pass);
                        }
                    }
                });


        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        if(ct!=null){
                            if(sendingSSIDThread!=null){
                                sendingSSIDThread.cancel();
                                sendingSSIDThread = null; }
                            ct.cancel();
                            ct = null;
                        }

                        dialog.cancel();
                    }
                });

        alertDialog.show();



    }






    // BLUETOTH SENDING DATA METHODS

    //Handler to handle Messages from the BTE Thread
    private Handler mmHandler = new Handler(){

        @Override
        public void handleMessage(@NonNull Message msg) {
            //Bundle bundle = msg.getData();
            // String _msg = bundle.getString(MSG_KEY);

            int numBytes = msg.arg1; //Buffer size
            byte[] mmBuffer = (byte[])msg.obj; //Buffer


            if(numBytes>0){
                byte[] readBuffer = new byte[numBytes]; //Buffer de lectura
                for(int i=0; i<numBytes; i++){
                    readBuffer[i] = mmBuffer[i];
                }
                String readed = null;
                try {

                    //Cancels dialog
                    if(dialog!=null){
                        dialog.dismiss();
                    }

                    readed = new String(readBuffer, "UTF-8");
                    Log.e("BTE", "Recieved: "+readed);

                    final Snackbar snackbar;

                    switch (readed){
                        case NET_OK:
                            //Toast.makeText(getContext(), "Data successfully sended to Terrarium", Toast.LENGTH_LONG).show();
                            snackbar = Snackbar.make(constraintLayout, "Data successfully sended to Terrarium", Snackbar.LENGTH_INDEFINITE);
                            snackbar.setAction("Close", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    snackbar.dismiss();
                                }
                            });
                            snackbar.show();
                            successfullACK = true; // Conexión con el nodo correcta
                            break;

                        case NET_CONN_FAIL:
                            //Toast.makeText(getContext(), "WiFi connection failed, credentials or broadcast failed", Toast.LENGTH_LONG).show();
                            snackbar = Snackbar.make(constraintLayout, "WiFi connection failed, credentials or broadcast failedm", Snackbar.LENGTH_INDEFINITE);
                            snackbar.setAction("Close", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    snackbar.dismiss();
                                }
                            });
                            snackbar.show();
                            successfullACK = true; // Conexión con el nodo correcta (aunque el WiFi este incorrecto)
                            break;
                        default:
                            //Toast.makeText(getContext(), "Device fail, please check the selected device it's a MyEarth Terrarium", Toast.LENGTH_LONG).show();
                            // BLE Noise from other devices

                            if(readed.contains("[ACK_STATUS]")){
                                String m = "Node Status: ";
                                m+=readed;
                                Log.i("RECIEVED", readed);
                                snackbar = Snackbar.make(constraintLayout, m, Snackbar.LENGTH_INDEFINITE);
                                snackbar.setAction("Close", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        snackbar.dismiss();
                                    }
                                });
                                snackbar.show();
                                successfullACK = true; // Conexión con el nodo correcta
                            }



                            break;
                    }


                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace(); //Not UTF-8
                }


            }

            if(numBytes<0){
                try{
              final Snackbar snackbar = Snackbar.make(constraintLayout, "No response recieved from the other device", Snackbar.LENGTH_INDEFINITE);
                snackbar.setAction("Close", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        snackbar.dismiss();
                    }
                });

                /*if(sendingSSIDThread.getMmSocket()!=null){
                    if(sendingSSIDThread.getMmSocket().isConnected()){
                        String _name = sendingSSIDThread.getMmSocket().getRemoteDevice().getName();
                        if (_name.contains("MyNode")){
                            snackbar.setText("Already connected to this device");
                        }
                    }
                    snackbar.show();
                }*/

                }catch (IllegalArgumentException ex){
                    //Do nothing
                }
            }




        }
    };






    public void establishConnection(){

        //Conexión limpia
        cleanConnection();

        Log.d("Dev", "Device:"+device.getAddress());

        boolean success = false;
        if(this.device.getBondState() != BluetoothDevice.BOND_BONDED){
            success = this.device.createBond();
        } else{ success = true; } //Device already bonded

        if(success){

            ct = new ConnectThread(device, this.bta);
            ct.start();

            //Liberates the server socket
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    Log.i("BLE", "Attempt to connect as client");

                    sendingSSIDThread = new ConnectedThread(ct.getMmSocket(), mmHandler);
                    sendingSSIDThread.start();


                    BluetoothGattCallback callback = new BluetoothGattCallback() {
                        @Override
                        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
                            super.onConnectionStateChange(gatt, status, newState);

                            //Connectivity icon
                          /*  getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if(sendingSSIDThread.getMmSocket().isConnected()){
                                        PairedDevicesAdapter.ViewHolder v = (PairedDevicesAdapter.ViewHolder) rv.findViewHolderForAdapterPosition(bte_choosed_pos);
                                        //v.img_connected.setVisibility(View.VISIBLE);

                                    }
                                }
                            }); //End update the connectivity icon*/


                        }
                    };
                    devgatt = device.connectGatt(getContext(), true, callback);

                }
            }, 2000);


        }

    }





    public void sendConfig(final String SSID, final String psswd){

        Handler handler = new Handler();

        startDialog(); //Shows dialog
        final Handler h = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                h.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(dialog!=null){
                            dialog.dismiss();

                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if(!successfullACK){ //Solo se ejecuta si no se llega a encontrar ningún nodo que responda a la comunicación Bluetooth
                                        final Snackbar snackbar;
                                        snackbar = Snackbar.make(constraintLayout, "Failed. Terrarium node not detected", Snackbar.LENGTH_INDEFINITE);
                                        snackbar.setAction("Close", new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                snackbar.dismiss();
                                            }

                                        });

                                        snackbar.show();
                                    } // Fin comprobación
                                    else{
                                        successfullACK = false; // Se vuelve a marcar como falso la comunicación al dar por superado el envío
                                    }
                                }
                            });

                        }
                    }
                }, 20000);
            }
        }).start();


        if(sendingSSIDThread==null){
            establishConnection();
        }
        if(sendingSSIDThread!=null){
            if(!sendingSSIDThread.getMmSocket().isConnected()){
                establishConnection();
            }
        }

        //Dev gate
        final BluetoothGattCallback callback = new BluetoothGattCallback() {
            @Override
            public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
                super.onConnectionStateChange(gatt, status, newState);
                Log.i("connection", "Status: "+newState);
            }
        };

        if(devgatt!=null){
            if(( mbluetoothManager.getConnectionState(device, BluetoothProfile.GATT) != BluetoothProfile.STATE_CONNECTING) &&
                    (mbluetoothManager.getConnectionState(device, BluetoothProfile.GATT) != BluetoothProfile.STATE_CONNECTED)) {

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        devgatt = device.connectGatt(getContext(), true, callback); //Connect gate
                    }
                },1500);

            }
        } else{
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    devgatt = device.connectGatt(getContext(), true, callback); //Connect gate
                }
            },1500);
        }



        handler.postDelayed(new Runnable() {
            public void run() {
                Log.i("ATTEMTP TO SEND...", "SENDING DATA");
                sendingSSIDThread = new ConnectedThread(ct.getMmSocket(), mmHandler);
                sendingSSIDThread.start();

                Handler h2 = new Handler();
                h2.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        writeSSIDParams(SSID, psswd);
                    }
                }, 2000);




            }
        }, 2000);
    }



    public void cleanConnection(){
        if(ct!=null){
            if(sendingSSIDThread!=null){
                sendingSSIDThread.cancel(); sendingSSIDThread = null;
            }
            if(devgatt!=null){
                devgatt.disconnect(); devgatt = null;
            }

            ct.cancel(); ct = null;
        }

    }



    //Write SSID params by bluetooth socket
    public void writeSSIDParams(String SSID, String psswd){

        String guid;
        FirebaseUser fu = FirebaseAuth.getInstance().getCurrentUser();
        if(fu!=null){
            guid = fu.getUid();
            this.sendingSSIDThread.write("[NET]".getBytes());
            this.sendingSSIDThread.write("[SSID]".getBytes()); this.sendingSSIDThread.write(SSID.getBytes());
            this.sendingSSIDThread.write("[PASS]".getBytes()); this.sendingSSIDThread.write(psswd.getBytes());
            this.sendingSSIDThread.write("[GUID]".getBytes()); this.sendingSSIDThread.write(guid.getBytes());
            this.sendingSSIDThread.write("[END]".getBytes());
        }
    }



    //Sends via BLE a "Status" request
    public void reportDeviceStatus(){
        this.sendingSSIDThread.write("[STATUS]".getBytes());
    }



    // Ifs

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        }else{
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }


    }




    private void startDialog(){

        // Check if no view has focus:
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        //Clear dialog first
        if(this.dialog!=null){
            if(this.dialog.isShowing()){
                this.dialog.dismiss();}
        }

        //Creates dialog
        this.dialog = ProgressDialog.show(getContext(), "Sync", "Trying to send data to terrarium node", true);

    }




}
