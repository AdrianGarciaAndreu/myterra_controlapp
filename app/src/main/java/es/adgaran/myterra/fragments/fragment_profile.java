package es.adgaran.myterra.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Picasso;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import es.adgaran.myterra.R;

public class fragment_profile extends Fragment {

    FirebaseUser user;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);
        View root = inflater.inflate(R.layout.fragment_profile, container, false);


        user = FirebaseAuth.getInstance().getCurrentUser();
        String name = user.getDisplayName();
        String mail = user.getEmail();
        String photoUrl = user.getPhotoUrl().toString();

        TextView tv1 = root.findViewById(R.id.prof_name);
        TextView tv2 = root.findViewById(R.id.prof_email);
        ImageView iv = root.findViewById(R.id.prof_img);

        tv1.setText(name);
        tv2.setText(mail);

        Picasso.get().load(user.getPhotoUrl()).into(iv);

        return root;
    }





    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
