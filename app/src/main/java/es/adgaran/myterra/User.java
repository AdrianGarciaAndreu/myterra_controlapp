package es.adgaran.myterra;

import android.util.Log;

import java.text.DateFormat;
import java.util.Date;

public class User {

    private String nombre;
    private String guid;
    private String correo;
    private String telefono;
    private String direccion;
    private String pais;
    private long inicioSesion;


    /**
     * Clase para gestionar la información de los usuarios como un objeto con firebase
     */

    public User(String guid, String name, String mail, long loginTime){
        this.nombre = name; this.correo = mail; this.guid = guid;
        this.inicioSesion = loginTime;
    }

    public User (String guid, String nombre, String correo, String telefono, String direccion, String pais, long inicioSesion) {
        this.nombre = nombre;
        this.correo = correo;
        this.telefono = telefono;
        this.direccion = direccion;
        this.pais = pais;
        this.inicioSesion = inicioSesion;
        this.guid = guid;
    }


    public String getTelefono() {
        return telefono;
    }

    public void setTelefono (String telefono) {
        this.telefono = telefono;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public Date getInicioSession() {
        Date d = new Date(); d.setTime(this.inicioSesion);
        //Log.e("Tiempo", this.inicioSesion+": "+d);
        return d;

    }

    public void setInicioSession(Date d) {
        this.inicioSesion = d.getTime();
    }


    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public long getInicioSesion() {
        return inicioSesion;
    }

    public void setInicioSesion(long inicioSesion) {
        this.inicioSesion = inicioSesion;
    }
}
