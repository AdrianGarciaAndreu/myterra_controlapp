package es.adgaran.myterra.services;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import es.adgaran.myterra.RemoteCallback;
import es.adgaran.myterra.remotes.NodesGET;

public class BGService  extends IntentService implements RemoteCallback {

    public static final String ACTION_MyIntentService = "es.adgaran.myterra.BG";
    public static final String EXTRA_KEY_IN = "EXTRA_IN";
    public static final String EXTRA_KEY_OUT = "EXTRA_OUT";

    String msgFromActivity;
    String extraOut;
    private final int waitTime = 5000;


    public BGService(){
        super("es.adgaran.myterra.services.BGService");
    }

    public BGService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {


        //get input
        msgFromActivity = intent.getStringExtra("EXTRA_IN");
        extraOut = "Hello: " +  msgFromActivity;

        while(true){

            try {
                Thread.sleep(waitTime);

                // Do in background retrieve data from BBDD
                processWork();

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


    }

    /**
     * Get NODE info in background
     */
    private void processWork(){

        String guid;
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        try{
            Log.d("GUID", user.getUid());
            if(user.getUid().length()>0){
                guid = user.getUid();

                Log.i("SEARCHING BG", "Searching for nodes in Background");

                // GET REQUEST
                NodesGET nodesGet = new NodesGET(guid, (RemoteCallback) this);
                nodesGet.execute();

            }

        } catch (NullPointerException ex){
            ex.printStackTrace();
        }

    }


    /**
     * Sends data via broadcast receiver to the home fragment to update the ui
     * if needed
     * @param data
     */
    public void sendDataToActivity(String data){
        //return result
        Intent intentResponse = new Intent();
        intentResponse.setAction(ACTION_MyIntentService);
        intentResponse.addCategory(Intent.CATEGORY_DEFAULT);

        intentResponse.putExtra(EXTRA_KEY_OUT, data);

        sendBroadcast(intentResponse);


    }


    @Override
    public void successfullOP(int requestCode, String data) {

        sendDataToActivity(data);
        Log.d(ACTION_MyIntentService, data);

    }

    @Override
    public void failedOP(int requestCode) {

        Log.e(ACTION_MyIntentService, "Error gathering online data");

    }
}
