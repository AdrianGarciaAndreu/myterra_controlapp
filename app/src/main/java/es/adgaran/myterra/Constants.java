package es.adgaran.myterra;

public class  Constants {

   // public final static String API_URL ="http://192.168.0.10/myTerra/API/V1.0/";
   public final static String API_URL ="https://iotmyterra.000webhostapp.com/myTerra/API/V1.0/";

    // REQUEST CODES
    public final static int COD_REMOTE_NODE_GET = 10;
    public final static int COD_REMOTE_NODE_POST = 11;

    public final static int COD_REMOTE_USER_POST = 12;
    public final static int COD_REMOTE_USER_GET = 13;


}
