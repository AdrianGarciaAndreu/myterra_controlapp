package es.adgaran.myterra.remotes;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import es.adgaran.myterra.Constants;
import es.adgaran.myterra.RemoteCallback;
import es.adgaran.myterra.User;
import es.adgaran.myterra.pojo.Node;

public class UserPOST extends AsyncTask<Void, Void, String> {

    private final int REQUEST_COD = Constants.COD_REMOTE_USER_POST;

    private final String RESTResource = "user";
    private String params;
    private User u;

    private RemoteCallback callback;



    public UserPOST(User u, RemoteCallback callback){

        this.callback = callback;
        this.u = u;

        this.params = obtenerParametros(u);

    }




    private String obtenerParametros(User u){

        //Se añaden los parametros del request a la API REST como clave Valor
        HashMap<String, Object> parametros = new HashMap<>();


        // Campos obligatorios
        parametros.put("guid", u.getGuid());

        parametros.put("name", u.getNombre());
        parametros.put("email", u.getCorreo());

        // Se recorre el objeto clave valor y se insertan los valores en un Texto
        Iterator it = parametros.entrySet().iterator();
        boolean primero = true;
        String parametrosString = "";
        while (it.hasNext()){
            if(primero){
                primero = false;
            } else{
                parametrosString +="&"; //Se separan los datos con el caracter '&'
            }

            Map.Entry pair = (Map.Entry)it.next();
            parametrosString +=pair.getKey()+"="+pair.getValue(); //A cada dato se le asigna el valor tras el caracter '='
        }

        //Se devuelven los datos con sus valores como Texto
        return parametrosString;

    }




    @Override
    protected String doInBackground(Void... voids) {
        String result = null;

        URL url;
        HttpURLConnection connection = null;

        try {
            // ---------------------------------------
            url = new URL(Constants.API_URL + RESTResource );

            // Crea una conexion HTTP para la url determinada
            connection = (HttpURLConnection) url.openConnection();


            // Se establece el tipo de la conexion
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("Accept", "*/*");


            // Escribte los datos a enviar en la peticion REST de tipo POST
            connection.setDoOutput(true);
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream()));
            writer.write(params);
            writer.close();


            // Captura la respuesta
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String linea="", resultado = "";
            while ((linea = reader.readLine()) !=null){
                resultado += linea;
            }
            //Devuelve el resultado
            if(resultado.length()>0){
                result = resultado;
            }

            // Establece la conexion
            connection.connect();


            //Muestra por consola la respuesta
            Log.d("User", "Obtained: " + connection.getResponseMessage() + "");

        } catch (Exception e) {
            Log.e(e.toString(), "Fallo en la solicitud");
        } finally {
            // Se asegura el cierre de conexion, en caso de haberla establecido
            if(connection!=null){
                connection.disconnect();
            }
        }

        return result;

    }


    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        // Si el resultado es el esperado...
        if(result != null){
            if(result.length()>0)
                callback.successfullOP(REQUEST_COD, result);
        } else{
            callback.failedOP(REQUEST_COD);
        }



    }


}
