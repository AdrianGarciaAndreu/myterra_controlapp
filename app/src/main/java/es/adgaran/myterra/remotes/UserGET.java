package es.adgaran.myterra.remotes;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import es.adgaran.myterra.Constants;
import es.adgaran.myterra.RemoteCallback;
import es.adgaran.myterra.User;

public class UserGET extends AsyncTask<Void, Void, String> {

    private final int REQUEST_COD = Constants.COD_REMOTE_USER_GET;

    private final String RESTResource = "user";
    private String params;
    private User u;

    private RemoteCallback callback;



    /**
     * GET Request, user's node
     * @param guid = user GUID
     */
    public UserGET(String guid, RemoteCallback callback){
        this.params = "guid="+guid;
        this.callback = callback;
    }


    @Override
    protected String doInBackground(Void... voids) {
        String result = null;

        URL url;
        HttpURLConnection connection = null;

        try {
            // ---------------------------------------
            // Peticion GET datos en la URL
            url = new URL(Constants.API_URL + RESTResource + "?" + params);
            Log.e("REQUEST", url.toString());


            // Crea una conexion HTTP para la url determinada
            connection = (HttpURLConnection) url.openConnection();

            // Se establece el tipo de la conexion
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Accept", "*/*");


            // Captura la respuesta
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String linea="", resultado = "";
            while ((linea = reader.readLine()) !=null){
                resultado += linea;
            }
            //Devuelve el resultado
            if(resultado.length()>0){
                result = resultado;
            }


            // Establece la conexion
            connection.connect();

            //Muestra por consola la respuesta
            Log.d("Node", "Obtained: " + connection.getResponseMessage() + "");

        } catch (Exception e) {
            Log.e(e.toString(), "Fallo en la solicitud");
        } finally {
            // Se asegura el cierre de conexion, en caso de haberla establecido
            if(connection!=null){
                connection.disconnect();
            }
        }

        return result;

    }


    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        // Si el resultado es el esperado...
        if(result != null){
            if(result.length()>0)
                callback.successfullOP(REQUEST_COD, result);
        } else{
            callback.failedOP(REQUEST_COD);
        }



    }





}
